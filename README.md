# PHP Markdown minisite



## Getting started

Clone this repository.

Use composer to install dependencies `composer install`.

You can edit `config/settings.json` to override path locations.

By default create `pages` directory and add markdown file into. You can have 1 directory level, this directories are menu elements.

In each directory you can create `init` file with Markdown title line (# My title), this line is title of menu and page.
