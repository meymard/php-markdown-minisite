<?php
    // Header

    $headerFile = sprintf('%s/../_header.php', dirname(__FILE__));

    //Menu items
    $baseDir = PAGES_LOCATION;
    $menuItems = [];
    if (file_exists($baseDir)) {
        $files = scandir($baseDir);
        foreach ($files as $f) {
            $realPath = $baseDir . $f;
            if (is_dir($realPath) && strpos($f, '.') !== 0) {
                if (is_file($initFile = $realPath . DIRECTORY_SEPARATOR . 'init')) {
                    $init = file_get_contents($initFile);
                    preg_match('/^# (.*)$/m', $init, $matches);
                    $name = isset($matches[1]) ? $matches[1] : null;
                } else {
                    $name = $f;
                }
                $menuItems[$f] = $name;
            }
        }
    }

?>
<header class="content">
<?php $baseDir = dirname(__FILE__).'/..' ?>
<?php if ( file_exists($baseDir)): ?>
<nav>
    <fieldset>
        <legend>Menu</legend>
        <ul id="menu">
        <li><a href="<?php print $base; ?>/"<?php print ($file == '.') ? ' class="current"' : ''; ?>>Accueil</a></li>
        <?php foreach ($menuItems as $path => $title) : ?>
            <li><a href="<?php print $base; ?>?file=<?php print $path; ?>"<?php print ($file == $path) ? ' class="current"' : ''; ?>><?php print $title; ?></a></li>
        <?php endforeach; ?>
        </ul>
    </fieldset>
</nav>
<?php endif; ?>
<div id="header">
    <?php
    if (file_exists($headerFile)) {
        include($headerFile);
    }
    ?>
    <form action="<?php print $base; ?>/search.php" method="GET">
    <input type="text" name="text" value="<?php print isset($search) ? $search : '' ?>" /><button type="submit">Rechercher</button>
    </form>
</div>
</header>
