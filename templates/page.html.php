<?php $basePath = $basePath ?? null; ?>
<?php $content = $content ?? null; ?>
<?php $currentUri = $currentUri ?? null; ?>
<html>
    <head>
        <title>Mon site</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="<?php print $assetsBasePath; ?>assets/base.css" type="text/css"/>
        <link rel="stylesheet" href="<?php print $assetsBasePath; ?>assets/print.css" type="text/css" media="print"/>
        <link rel="shortcut icon" type="image/png" href="<?php print $assetsBasePath; ?>assets/icon.png"/>
        <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    </head>
    <body>
        <header class="content">
            <nav>
                <fieldset>
                    <legend>Menu</legend>
                    <ul id="menu">
                    <li><a href="<?php print $basePath; ?>"<?php print ($currentUri == $basePath) ? ' class="current"' : ''; ?>>Accueil</a></li>
                    <?php foreach ($menuItems as $path => $title) : ?>
                        <li>
                            <a href="<?php print $basePath . $path; ?>"<?php print ($currentUri == $basePath . $path) ? ' class="current"' : ''; ?>><?php print $title; ?></a>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </fieldset>
            </nav>
            <div id="header">
                <div id="logo">
                </div>
                <div id="header">
                    <p>
                        <a href="<?php print $basePath; ?>">
                            Marc EYMARD
                            <br />
                            Développeur WEB
                        </a>
                    </p>
                </div>
            </div>
        </header>
            <div class="content">
                <article>
                    <?php
                        print $content;
                    ?>
                </article>
            </div>
        </div>
    </body>
</html>
