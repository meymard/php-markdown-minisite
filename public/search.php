<?php
    // Afficher les erreurs à l'écran
    ini_set('display_errors', 1);
    // Enregistrer les erreurs dans un fichier de log
    ini_set('log_errors', 1);
    // Nom du fichier qui enregistre les logs (attention aux droits à l'écriture)
    error_reporting(E_ALL);

    $base = (isset($_SERVER['BASE']) && $_SERVER['BASE']) ? $_SERVER['BASE'] : '/';
    $base = ($base == '/') ? '' : $base;

    if (isset($_GET['text']) && $_GET['text']) {
        $search = $_GET['text'];
    } else {
        header(sprintf('Location: %s/', $base));
    }

    $dirPath = dirname(__FILE__).'/../';

    $output = [];
    $command = sprintf("grep -Rin --include='*.md' --exclude='.*' --exclude='{vendor,web,README.md}' '%s' %s", $search, $dirPath);
    exec($command, $output);

    $matches = [];
    foreach ($output as $result) {
        list($file, $line, $match) = explode(':', $result);
        $title = null;
        if (preg_match('/^\# (.*)$/m', file_get_contents($file), $matchesTitle)) {
            $title = $matchesTitle[1];
        }

        $filePath = str_replace($dirPath, '', $file);
        if (!isset($matches[$filePath])) {
            $matches[$filePath] = [
                'title' => $title,
                'matches' => [],
            ];
        }
        $matches[$filePath]['matches'][] = [
            'line' => $line,
            'content' => $match,
        ];
    }
    // No menu path
    $file = null;
?>
<html>
<head>
    <title>Recherche de <?php print $search ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="<?php print $base; ?>/base.css" type="text/css"/>
    <link rel="stylesheet" href="<?php print $base; ?>/print.css" type="text/css" media="print"/>
    <link rel="shortcut icon" type="image/png" href="<?php print $base; ?>/icon.png"/>
    <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
</head>
<body>
<div class="main">
    <?php include(dirname(__FILE__).'/_menu.php') ?>
    <div class="content">
        <article>
        <h1>Resultat pour "<?php print $search ?>"</h1>
        <?php foreach ($matches as $path => &$lines): ?>
            <a href="<?php print $base; ?>/?file=<?php print urlencode($path); ?>">
                <h2><?php print $lines['title'] ?: $path; ?></h2>
            </a>
            <ul>
            <?php foreach ($lines['matches'] as $match): ?>
                <li><?php print $match['content'] ?></li>
            <?php endforeach; ?>
            </ul>
        <?php endforeach; ?>
        </article>
    </div>
</div>
</body>
</html>
