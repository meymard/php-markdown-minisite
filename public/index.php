<?php
    // Afficher les erreurs à l'écran
    ini_set('display_errors', 1);
    // Enregistrer les erreurs dans un fichier de log
    ini_set('log_errors', 1);
    // Nom du fichier qui enregistre les logs (attention aux droits à l'écriture)
    error_reporting(E_ALL);

    require dirname(__FILE__).'/../vendor/autoload.php';

    $configPath = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'settings.json';

    $app = new Site\App($configPath, $_SERVER);

    try {
        print $app->run();
    } catch (Throwable $e) {
        print $app->showException($e);
    }
