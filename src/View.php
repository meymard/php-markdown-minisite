<?php

namespace Site;

class View
{
    protected File $file;
    protected static ?Menu $menu = null;

    public function __construct(File $file)
    {
        $this->file = $file;
    }

    public static function setMenu(Menu $menu): void
    {
        self::$menu = $menu;
    }

    public function getPage(): string
    {
        if ($this->file->isDir()) {
            $pages = $this->file->getMarkdownFilesFromDir();
        } else {
            $pages = [$this->file];
        }

        $content = '';
        foreach ($pages as $page) {
            $content .= $page->getHtmlFromMarkdown();
        }

        return $content;
    }

    public static function loadTemplate(?string $template, array $vars): string
    {
        $menuItems = self::$menu ? self::$menu->getMenu() : [];
        foreach ($vars as $name => $value) {
            ${$name} = $value;
        }

        $templatePath = __DIR__ . DIRECTORY_SEPARATOR
            .'..'. DIRECTORY_SEPARATOR
            . 'templates'
            . DIRECTORY_SEPARATOR
            . ($template ?: "page.html.php");

        ob_start();

        include $templatePath;

        $content = ob_get_contents();

        ob_end_clean();

        return $content;
    }

    protected function getPageTitle(): ?string
    {
        return $this->file->getGlobalTitle();
    }
}

