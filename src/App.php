<?php

namespace Site;

class App
{
    protected Config $config;
    protected Router $router;
    protected array $serverData;
    protected ?string $basePath = null;

    public function __construct($configPath, array $serverData)
    {
        $this->serverData = $serverData;
        $this->config = new Config($configPath);
        $this->router = new Router($this->config->getConfig(Config::PAGES_LOCATION_CONFNAME));
        View::setMenu(new Menu($this->config->getConfig(Config::PAGES_LOCATION_CONFNAME)));
    }

    public function run(): string
    {
        $uri = preg_replace("#^{$this->getBasePath()}#", '', $this->getCurrentUri());

        $pagePath = $this->router->getFilePathFromUri($uri) ?: '';

        $file = new File($pagePath);
        $view = new View($file);

        $content = $view->getPage();

        return View::loadTemplate(null, [
            'basePath' => $this->getBasePath(),
            'assetsBasePath' => preg_replace('#index.php/#', '', $this->getBasePath()),
            'content' => $content,
            'currentUri' => $this->getCurrentUri(),
        ]);
    }

    public function showException(\Throwable $e): string
    {
        $errorCode = $e->getCode();
        if ($errorCode < 100 || $errorCode >= 600) {
            $errorCode = 500;
        }

        http_response_code($errorCode);

        $errorsLocation = $this->config->getConfig(Config::ERRORS_PAGES_LOCATION_CONFNAME);

        $pagePath = __DIR__ . "/../{$errorsLocation}/{$errorCode}.md";
        if (!file_exists($pagePath)) {
            $pagePath = __DIR__ . "/../{$errorsLocation}/default.md";
        }

        try {
            $file = new File($pagePath);
            $view = new View($file);

            $content = $view->getPage();
        } catch (\Exception $e) {
            $content = $e->getMessage();
        }

        return View::loadTemplate(null, [
            'basePath' => $this->getBasePath(),
            'content' => $content,
            'currentUri' => $this->getCurrentUri(),
        ]);
    }

    public function getCurrentUri(): string
    {
        return $this->serverData['REQUEST_URI'] ?? '';
    }

    public function getBasePath(): string
    {
        if (null === $this->basePath) {
            $scriptName = $this->serverData['SCRIPT_NAME'] ?? null;
            $this->basePath = $scriptName; // ? dirname($scriptName) : '';
            if (!preg_match('#/$#', $this->basePath)) {
                $this->basePath = $this->basePath . '/';
            }
        }

        return $this->basePath;
    }
}
