<?php

namespace Site;

class Menu
{
    protected Config $config;
    protected string $pagesLocation;

    public function __construct(string $pagesLocation)
    {
        $this->pagesLocation = $pagesLocation;
    }

    /** @return string[] */
    public function getMenu(): array
    {
        $pageDir = __DIR__ . DIRECTORY_SEPARATOR
            . '..' . DIRECTORY_SEPARATOR
            . $this->pagesLocation . DIRECTORY_SEPARATOR;

        $dir = [];
        if (is_dir($pageDir)) {
            $files = scandir($pageDir);
            foreach ($files as $f) {
                $realPath = $pageDir . $f;
                if (is_dir($realPath) && strpos($f, '.') !== 0) {
                    if (is_file($initFile = $realPath . DIRECTORY_SEPARATOR . 'init')) {
                        $init = file_get_contents($initFile);
                        preg_match('/^# (.*)$/m', $init, $matches);
                        $name = isset($matches[1]) ? $matches[1] : null;
                    } else {
                        $name = $f;
                    }
                    $dir[$f] = $name;
                }
            }

            return $dir;
        }
    }
}
