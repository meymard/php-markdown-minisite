<?php

namespace Site;

class Router
{
    protected const MARKDOWN_REGEX_FILE = '/\.md$/';
    protected string $pagesLocation;

    public function __construct(string $pagesLocation)
    {
        $this->pagesLocation = $pagesLocation;
    }
    public function getFilePathFromUri(string $uri): ?string
    {
        if (strpos('/', $uri) !== 0) {
            $uri = "/{$uri}";
        }
        $filePath = __DIR__ . "/../{$this->pagesLocation}{$uri}";

        if (!file_exists($filePath)) {
            return null;
        }

        return $filePath;
    }
}
