<?php

namespace Site;

class Config
{
    public const PAGES_LOCATION_CONFNAME = 'pagesLocation';
    public const ERRORS_PAGES_LOCATION_CONFNAME = 'errorsPagesLocation';

    protected array $configs;

    public function __construct(string $configPath = null)
    {
        $this->loadFile($configPath);
    }

    public function loadFile(string $configPath): void
    {
        if (!is_file($configPath) && !is_readable($configPath)) {
            throw new \Exception('File no exist or not readable', 404);
        }
        try {
            $this->configs = json_decode(file_get_contents($configPath), true);
        } catch (\Throwable $e) {
            throw new \Exception("Error on read file : {$e->getMessage()}", 500);
        }

        if (!$this->validData($this->configs)) {
            throw new \Exception("Data incorect.", 500);
        }
    }

    /**
     * @return mixed
     */
    public function getConfig(string $name)
    {
        return $this->configs[$name] ?? null;
    }

    protected function validData(array $data): bool
    {
        if (!($data[self::PAGES_LOCATION_CONFNAME] ?? null)) {
            return false;
        }

        return true;
    }
}

