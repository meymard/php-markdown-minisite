<?php

namespace Site;

use League\CommonMark\CommonMarkConverter;

class File
{
    protected const MARKDOWN_REGEX_FILE = '/\.md$/';

    protected CommonMarkConverter $converter;

    protected string $filePath;

    protected bool $isDir;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;

        if ($filePath && !file_exists($filePath)) {
            throw new \Exception('File not exist', 404);
        }

        $this->isDir = is_dir($filePath);

        $this->converter = new CommonMarkConverter([
            'html_input' => 'strip',
            'allow_unsafe_links' => false,
        ]);
    }

    public function isDir(): bool
    {
        return $this->isDir;
    }

    public function getContent(): string
    {
        return \file_get_contents($this->filePath);
    }

    public function getHtmlFromMarkdown(): string
    {
        if (!is_file($this->filePath) || !preg_match('/\.md$/', $this->filePath)) {
            throw new \Exception('File not found or not a markdown file', 404);
        }

        return $this->converter->convert(file_get_contents($this->filePath));
    }

    /**
     * @return array of Files
     */
    public function getMarkdownFilesFromDir(): array
    {
        if (!$this->isDir()) {
            throw new \Exception('Path not a dir', 400);
        }

        $pages = [];

        $files = scandir($this->filePath, \SCANDIR_SORT_ASCENDING);
        foreach ($files as $file) {
            $fullPath = sprintf('%s/%s', $this->filePath, $file);
            if (is_dir($fullPath)) {
                continue;
            }

            if (is_file($fullPath) && preg_match('/\.md$/', $file)) {
                $pages[] = new File($fullPath);
            }
        }

        return $pages;
    }

    public function getGlobalTitle(): ?string
    {
        if (!$this->isDir()) {
            return null;
        }

        try {
            $iniFile = new File(sprintf('%s/ini', $this->filePath));
            preg_match('/^# (.*)$/m', $iniFile->getContent(), $matches);

            return $matches[1] ?? null;
        } catch (\Exception $e) {
            return null;
        }
    }
}

